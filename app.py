from flask import Flask, render_template, redirect, request
from wtforms import Form, StringField, IntegerField
from datetime import datetime
from flask_sqlalchemy import SQLAlchemy

# init Flask
app = Flask(__name__)

# configuring our database uri
app.config["SQLALCHEMY_DATABASE_URI"] = "mysql+mysqlconnector://{user}@{server}/albis".format(user="root",
                                                                                              server="localhost")

# Init db abstraction
db = SQLAlchemy(app)


# Project Manager Class/Model
class ProjectManager(db.Model):
    __tablename__ = "projectmanagers"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    first_name = db.Column(db.String(50), unique=True)
    last_name = db.Column(db.String(50), unique=True)
    department = db.Column(db.String(50))

    def __init__(self, fname, lname, department):
        self.first_name = fname
        self.last_name = lname
        self.department = department


# Project Class/Model
class Project(db.Model):
    __tablename__ = "projects"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(50), unique=True)
    start = db.Column(db.DATE)
    end = db.Column(db.DATE)
    pm_id = db.Column(db.Integer, db.ForeignKey(ProjectManager.id))

    def __init__(self, name, start, end, pm_id):
        self.name = name
        self.start = start
        self.end = end
        self.pm_id = pm_id


# list of managers is an entry point
@app.route('/', methods=['GET'])
def index():
    project_managers = ProjectManager.query.all()
    return render_template('project_managers.html', project_managers=project_managers)


# Project Manager Form Class
class ProjectManagerForm(Form):
    first_name = StringField('First Name')
    last_name = StringField('Last Name')
    department = StringField('Department')


# add project manager
@app.route('/add_project_manager', methods=['POST', 'GET'])
def add_project_manager():
    form = ProjectManagerForm(request.form)
    if request.method == 'POST':
        first_name = form.first_name.data
        last_name = form.last_name.data
        department = form.department.data

        new_project_manager = ProjectManager(first_name, last_name, department)

        db.session.add(new_project_manager)
        db.session.commit()

        return redirect('/')

    return render_template('add_project_manager.html', form=form)


# Project Form Class
class ProjectForm(Form):
    name = StringField('Name')
    start = StringField('Start Date')
    end = StringField('End Date')
    pm_id = IntegerField('Project Manager ID')


# add project
@app.route('/add_project', methods=['POST', 'GET'])
def add_project():
    form = ProjectForm(request.form)
    if request.method == 'POST':
        name = form.name.data
        start = form.start.data
        end = form.end.data
        pm_id = form.pm_id.data

        # convert string to date, had problems with wtforms.Datefield
        start = datetime.strptime(start, "%Y-%m-%d").date()
        end = datetime.strptime(end, "%Y-%m-%d").date()

        new_project = Project(name, start, end, pm_id)

        db.session.add(new_project)
        db.session.commit()

        return redirect('/')

    return render_template('add_project.html', form=form)

# get projects managed by certain manager
@app.route('/dashboard/<pm_id>', methods=['GET'])
def pm_dashboard(pm_id):
    project_manager = ProjectManager.query.get(pm_id)
    projects = Project.query.filter_by(pm_id=pm_id)
    return render_template('dashboard.html', projects=projects, project_manager=project_manager)


if __name__ == '__main__':
    app.run()
